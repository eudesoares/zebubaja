package br.com.zebubaja.instrumentos;



import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import javax.imageio.ImageIO;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */





/**
 *
 * @author hansolo
 */
public final class RpmBar extends javax.swing.JComponent
{
    private float min = 1500;
    private float max = 4500;
    private String unitString = "Km/h";
    public enum luminosity {LIGHT, DARK};
    public luminosity brightness = luminosity.DARK;
    private java.awt.image.BufferedImage coverImage = null;
    private java.awt.image.BufferedImage fundo = null;
    private java.awt.image.BufferedImage pointerImage = null;
    private double offsetX =-0;
    private double offsetY = 0;
    private double rotationCenterX = 0;
    private double rotationCenterY = 0;
    private double value = 0;
    private final double factor =2*Math.PI*180/360/(max-min);
    private final double rotationOffset = 0;
    java.awt.geom.AffineTransform oldTransform;

    public RpmBar() throws IOException, URISyntaxException
    {
        super();
        setPreferredSize(new java.awt.Dimension(633, 351));
        setSize(633, 351);
        init();
    }

    private void init() throws IOException, URISyntaxException
    {
        if (coverImage != null)
        {
            coverImage.flush();
        }
        fundo = createInstrumentBack();
        if (fundo != null)
        {
            fundo.flush();
        }
        coverImage = createInstrumentCover();
        if (pointerImage != null)
        {
            pointerImage.flush();
        }
        pointerImage = createPointer();
        offsetX =1 ;
        offsetY =-1;
        rotationCenterX = pointerImage.getWidth() / 2;
        rotationCenterY = 320;
    }
    @Override
    protected void paintComponent(java.awt.Graphics g)
    {
        java.awt.Graphics2D g2 = (java.awt.Graphics2D) g;
        g2.setRenderingHint(java.awt.RenderingHints.KEY_ANTIALIASING, java.awt.RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(java.awt.RenderingHints.KEY_ALPHA_INTERPOLATION, java.awt.RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g2.setRenderingHint(java.awt.RenderingHints.KEY_COLOR_RENDERING, java.awt.RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2.setRenderingHint(java.awt.RenderingHints.KEY_STROKE_CONTROL, java.awt.RenderingHints.VALUE_STROKE_PURE);
        g2.setRenderingHint(java.awt.RenderingHints.KEY_DITHERING, java.awt.RenderingHints.VALUE_DITHER_ENABLE);
        g2.setRenderingHint(java.awt.RenderingHints.KEY_RENDERING, java.awt.RenderingHints.VALUE_RENDER_QUALITY);    
        g2.drawImage(fundo, 1, 0, this);
        oldTransform = g2.getTransform();
        g2.translate(offsetX, offsetY);
        //g2.setComposite(java.awt.AlphaComposite.SrcAtop);
        g2.rotate(rotationOffset + (value-min) * factor, rotationCenterX, rotationCenterY);
        g2.drawImage(pointerImage, 1, 0, this);
        
        g2.setTransform(oldTransform);  
        g2.rotate(0, rotationCenterX, rotationCenterY);
        g2.drawImage(coverImage, 0, -1, this);      
        g2.dispose();
    }

    private java.awt.image.BufferedImage createPointer() throws IOException, URISyntaxException
    {
        
        String diretorio = "/br/com/zebubaja/Imagens/rpm_off2.png";
        InputStream resource = getClass().getResourceAsStream(diretorio);
        BufferedImage imagemPonteiro;
        imagemPonteiro=ImageIO.read(resource);      
        return imagemPonteiro;
    }
    private java.awt.image.BufferedImage createInstrumentCover() throws IOException
    {
        String diretorio = "/br/com/zebubaja/Imagens/rpm_div.png";
        InputStream resource = getClass().getResourceAsStream(diretorio);
        BufferedImage imagemFundo;
        imagemFundo=ImageIO.read(resource);
        return imagemFundo;
    }      
        private java.awt.image.BufferedImage createInstrumentBack() throws IOException
    {
        String diretorio = "/br/com/zebubaja/Imagens/rpm_on.png";
        InputStream resource = getClass().getResourceAsStream(diretorio);
        BufferedImage imagemFundo;
        imagemFundo=ImageIO.read(resource);
        return imagemFundo;
    }    
    public void setValue(double value)
    {
        if (value < min)
        {
            value = min;
        }
        if (value > max)
        {
            value = max;
        }
        this.value = value;
        javax.swing.SwingUtilities.invokeLater(this::repaint);
    }
    public void setBrightness(luminosity brightness) throws IOException, URISyntaxException
    {
        this.brightness = brightness;
        init();
        repaint();
    }
    public String getUnitString()
    {
        return this.unitString;
    }
    public void setUnitString(String unitString) throws IOException, URISyntaxException
    {
        this.unitString = unitString;
        init();
    }
    @Override
    public void setSize(java.awt.Dimension dimension)
    {
        if (dimension.width >= dimension.height)
        {
            super.setSize(new java.awt.Dimension(dimension.width, dimension.width));
        }
        else
        {
            super.setSize(new java.awt.Dimension(dimension.height, dimension.height));
        }
        repaint();
    }
    @Override
    public void setSize(int width, int height)
    {
        if (width >= height)
        {
            super.setSize(width, width);
        }
        else
        {
            super.setSize(height, height);
        }
        repaint();
    }
    
    /**
     * @return the min
     */
    public float getMin() {
        return min;
    }

    /**
     * @param min the min to set
     */
    public void setMin(float min) {
        this.min = min;
    }

    /**
     * @return the max
     */
    public float getMax() {
        return max;
    }

    /**
     * @param max the max to set
     */
    public void setMax(float max) {
        this.max = max;
    }
    public double getValue() {
        return this.value;
    }
}
