/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zebubaja.bean;

/**
 *
 * @author Spider
 */
public class CarData {
    private int longitude;
    private int latitude;
    private int rpm;
    private float tensao;
    private int velocidade;
    private int velocidade_maxima;
    private int freio_cont;
    private int motor_temp;
    private int cvt_temp;
    private int rec_state;
    private int sd_state;
    private float distancia;
    private int nivel_combustivel;
    private int com_state;
    private int gps_state;
    private int led_box;
    /**
     * @return the longitude
     */
    public int getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the latitude
     */
    public int getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(int latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the rpm
     */
    public int getRpm() {
        return rpm;
    }

    /**
     * @param rpm the rpm to set
     */
    public void setRpm(int rpm) {
        this.rpm = rpm;
    }

    /**
     * @return the tensao
     */
    public float getTensao() {
        return tensao;
    }

    /**
     * @param tensao the tensao to set
     */
    public void setTensao(float tensao) {
        this.tensao = tensao;
    }

    /**
     * @return the velocidade
     */
    public int getVelocidade() {
        return velocidade;
    }

    /**
     * @param velocidade the velocidade to set
     */
    public void setVelocidade(int velocidade) {
        this.velocidade = velocidade;
    }

    /**
     * @return the velocidade_maxima
     */
    public int getVelocidade_maxima() {
        return velocidade_maxima;
    }

    /**
     * @param velocidade_maxima the velocidade_maxima to set
     */
    public void setVelocidade_maxima(int velocidade_maxima) {
        this.velocidade_maxima = velocidade_maxima;
    }

    /**
     * @return the freio_cont
     */
    public int getFreio_cont() {
        return freio_cont;
    }

    /**
     * @param freio_cont the freio_cont to set
     */
    public void setFreio_cont(int freio_cont) {
        this.freio_cont = freio_cont;
    }

    /**
     * @return the motor_temp
     */
    public int getMotor_temp() {
        return motor_temp;
    }

    /**
     * @param motor_temp the motor_temp to set
     */
    public void setMotor_temp(int motor_temp) {
        this.motor_temp = motor_temp;
    }

    /**
     * @return the cvt_temp
     */
    public int getCvt_temp() {
        return cvt_temp;
    }

    /**
     * @param cvt_temp the cvt_temp to set
     */
    public void setCvt_temp(int cvt_temp) {
        this.cvt_temp = cvt_temp;
    }

    /**
     * @return the rec_state
     */
    public int getRec_state() {
        return rec_state;
    }

    /**
     * @param rec_state the rec_state to set
     */
    public void setRec_state(int rec_state) {
        this.rec_state = rec_state;
    }

    /**
     * @return the sd_state
     */
    public int getSd_state() {
        return sd_state;
    }

    /**
     * @param sd_state the sd_state to set
     */
    public void setSd_state(int sd_state) {
        this.sd_state = sd_state;
    }
    
        /**
     * @param distancia the distancia to set
     */
    public void setDistancia(float distancia) {
        this.distancia=distancia;
    }

    /**
     * @return the distancia
     */
    public float getDistancia() {
        return distancia;
    }

    /**
     * @return the nivel_combustivel
     */
    public int getNivel_combustivel() {
        return nivel_combustivel;
    }

    /**
     * @param nivel_combustivel the nivel_combustivel to set
     */
    public void setNivel_combustivel(int nivel_combustivel) {
        this.nivel_combustivel = nivel_combustivel;
    }

    /**
     * @return the com_state
     */
    public int getCom_state() {
        return com_state;
    }

    /**
     * @param com_state the com_state to set
     */
    public void setCom_state(int com_state) {
        this.com_state = com_state;
    }

    /**
     * @return the gps_state
     */
    public int getGps_state() {
        return gps_state;
    }

    /**
     * @param gps_state the gps_state to set
     */
    public void setGps_state(int gps_state) {
        this.gps_state = gps_state;
    }

    /**
     * @return the led_box
     */
    public int getLed_box() {
        return led_box;
    }

    /**
     * @param led_box the led_box to set
     */
    public void setLed_box(int led_box) {
        this.led_box = led_box;
    }
    
    
}
