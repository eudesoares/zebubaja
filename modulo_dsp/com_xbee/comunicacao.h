bool leituraMsgCar(data_network* data){
  static XbeeMsg msg;                         //Cria uma mensagem para o xbee
  msg.data=(uint8_t*)data;                     //Passa o ponteiro do struct de dados do veículo
  int status_msg=xbee_read_msg(&msg);         //Captura o status da recepção do de dados do xbee
  switch(status_msg){                         //Verifica o status da recepção de pacotes de mensagens
    case STATUS_OK:                           //Recepção de mensagem foi um sucesso                   
      switch(msg.type){                       //Verifica o status da recepção de pacotes de mensagens
        case RECEIVED_PACKAGE:                //Verifica o tipo de pacote recebido
          Serial.println("\n");    
          return true;     
        break;
      }
      break;
    case STATUS_ERR_TIMEOUT:                  //Excedeu o tempo de espera pelo pacote
      break;
    case STATUS_ERR_BAD_DATA:                 //Falha na verificação do checksum do pacote recebido.
      break;  
  }
  return false; 
}

bool sendMsgCar(data_network* data){
  uint8_t ans1 = xbee_send_msg(addressBox,(uint8_t*)data,sizeof(*data));
  //uint8_t ans2 = xbee_send_msg(addressPad,(uint8_t*)data,sizeof(*data));
}

