/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author Spider
 */
public class SComm implements Runnable, SerialPortEventListener {
    private String peso;
    private InputStream entrada;
    @Override
    public void run(){

                try {
                    Thread.sleep(5);
                } catch (Exception e) {
                    System.out.println("Erro de Thred: " + e);
                }
        }
    @Override
    public void serialEvent(SerialPortEvent ev){       
                StringBuffer bufferLeitura = new StringBuffer();
                int novoDado = 0;
                switch (ev.getEventType()) {
                    case SerialPortEvent.BI:
                    case SerialPortEvent.OE:
                    case SerialPortEvent.FE:
                    case SerialPortEvent.PE:
                    case SerialPortEvent.CD:
                    case SerialPortEvent.CTS:
                    case SerialPortEvent.DSR:
                    case SerialPortEvent.RI:
                    case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                    break;
                    case SerialPortEvent.DATA_AVAILABLE:
                        //Novo algoritmo de leitura.
                        while(novoDado != -1){
                            try{
                                novoDado = entrada.read();
                                if(novoDado == -1){
                                    break;
                                }
                                if('\r' == (char)novoDado){
                                    bufferLeitura.append('\n');
                                }else{
                                    bufferLeitura.append((char)novoDado);
                                }
                            }catch(IOException ioe){
                                System.out.println("Erro de leitura serial: " + ioe);
                            }
                        }
                        setPeso(new String(bufferLeitura));
                        System.out.println(getPeso());

                    break;

                }

        }
    public void setPeso(String peso){
            this.peso = peso;
    }
    public String getPeso(){
            return peso;
    }
}
   
