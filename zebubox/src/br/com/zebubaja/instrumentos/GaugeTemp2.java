package br.com.zebubaja.instrumentos;



import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import javax.imageio.ImageIO;

/**
 *
 * @author hansolo
 */
public final class GaugeTemp2 extends javax.swing.JComponent
{
    private float min = 20;
    private float max = 140;
    /**
     * @return the min
     */
    public float getMin() {
        return min;
    }

    /**
     * @param min the min to set
     */
    public void setMin(float min) {
        this.min = min;
    }

    /**
     * @return the max
     */
    public float getMax() {
        return max;
    }

    /**
     * @param max the max to set
     */
    public void setMax(float max) {
        this.max = max;
    }
    private java.awt.image.BufferedImage backgroundImage = null;
    private java.awt.image.BufferedImage pointerImage = null;
    private double offsetX = 0;
    private double offsetY = 0;
    private double rotationCenterX = 0;
    private double rotationCenterY = 0;
    private double value = 0;
    private final double factor =-Math.PI/(getMax()-getMin());
    private final double rotationOffset = -Math.PI*-173/180;
    private final java.awt.image.BufferedImage digits = null;
    java.awt.geom.AffineTransform oldTransform;

    public GaugeTemp2() throws IOException, URISyntaxException
    {
        super();
        setPreferredSize(new java.awt.Dimension(250, 250));
        setSize(233, 290);
        init();
    }

    private void init() throws IOException, URISyntaxException
    {
        if (backgroundImage != null)
        {
            backgroundImage.flush();
        }
        backgroundImage = createInstrumentBackground();
        if (pointerImage != null)
        {
            pointerImage.flush();
        }
        pointerImage = createPointer();
        offsetX = 64;
        offsetY = 34;
        rotationCenterX = pointerImage.getWidth() / 2.0d;
        rotationCenterY = pointerImage.getHeight() - pointerImage.getWidth() / 2.0d;
    }
    @Override
    protected void paintComponent(java.awt.Graphics g)
    {
        java.awt.Graphics2D g2 = (java.awt.Graphics2D) g;
        g2.setRenderingHint(java.awt.RenderingHints.KEY_ANTIALIASING, java.awt.RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(java.awt.RenderingHints.KEY_ALPHA_INTERPOLATION, java.awt.RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g2.setRenderingHint(java.awt.RenderingHints.KEY_COLOR_RENDERING, java.awt.RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2.setRenderingHint(java.awt.RenderingHints.KEY_STROKE_CONTROL, java.awt.RenderingHints.VALUE_STROKE_PURE);
        g2.setRenderingHint(java.awt.RenderingHints.KEY_DITHERING, java.awt.RenderingHints.VALUE_DITHER_ENABLE);
        g2.setRenderingHint(java.awt.RenderingHints.KEY_RENDERING, java.awt.RenderingHints.VALUE_RENDER_QUALITY);

        g2.drawImage(backgroundImage, 0, 0, this);
        
        g2.drawImage(digits, (int)(getWidth() * 0.325f), (int)(getHeight() * 0.61f), this);
        
        oldTransform = g2.getTransform();

        g2.translate(offsetX, offsetY);
        //g2.setComposite(java.awt.AlphaComposite.SrcAtop);
        g2.rotate(rotationOffset + value* factor, rotationCenterX, rotationCenterY);
        g2.drawImage(pointerImage, 0, 0, this);

        g2.setTransform(oldTransform);
        //g2.drawImage(highlightImage, 0, 0, this);
        
        g2.dispose();
    }

    private java.awt.image.BufferedImage createPointer() throws IOException, URISyntaxException
    {
        String diretorio = "/br/com/zebubaja/Imagens/ponteiro_temp.png";
        InputStream resource = getClass().getResourceAsStream(diretorio);
        BufferedImage imagemPonteiro;
        imagemPonteiro=ImageIO.read(resource);      
        return imagemPonteiro;
    }
    private java.awt.image.BufferedImage createInstrumentBackground() throws IOException
    {
        String diretorio = "/br/com/zebubaja/Imagens/marcador_temp2.png";
        InputStream resource = getClass().getResourceAsStream(diretorio);     
        BufferedImage imagemFundo;
        imagemFundo=ImageIO.read(resource);
        return imagemFundo;
    }
    public void setValue(double value)
    {
        if (value < getMin())
        {
            value = getMin();
        }
        if (value > getMax())
        {
            value = getMax();
        }
        this.value = value-20;
        javax.swing.SwingUtilities.invokeLater(this::repaint);
    }
    @Override
    public void setSize(java.awt.Dimension dimension)
    {
        if (dimension.width >= dimension.height)
        {
            super.setSize(new java.awt.Dimension(dimension.width, dimension.width));
        }
        else
        {
            super.setSize(new java.awt.Dimension(dimension.height, dimension.height));
        }
        repaint();
    }
    @Override
    public void setSize(int width, int height)
    {
        if (width >= height)
        {
            super.setSize(width, width);
        }
        else
        {
            super.setSize(height, height);
        }
        repaint();
    }
    public double getValue() {
        return this.value;
    }
}
