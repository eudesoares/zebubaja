/*
 * IncFile1.h
 *
 * Created: 08/08/2017 10:22:08
 *  Author: dinho
 */

//tIPOS DE PACOTE DO XBEE
#define		BROADCAST			(uint64_t)0xFFFF
//Posi��o dos dados na mensafem de TRANSMIT_STATUS
#define		TX_RETRY_COUNT		0X03
#define		FRAME_ID_STATUS		0X00
#define		DELIVERY_STATUS		0x04
#define		DISCOVER_STATUS		0x05
enum status_code {
  STATUS_OK                         = 0x00,
  STATUS_VALID_DATA                 = 0x01,
  STATUS_NO_CHANGE                  = 0x02,
  STATUS_ABORTED                    = 0x04,
  STATUS_BUSY                       = 0x05,
  STATUS_SUSPEND                    = 0x06,
  STATUS_ERR_TIMEOUT                = 0x07,
  STATUS_ERR_BAD_DATA               = 0x08,
  STATUS_ERR_NOT_FOUND              = 0x09
};
typedef enum status_code status_code_genare_t;
struct xbee_at_msg
{
	uint8_t destination;	//0 local device	1 remote AT_command
	uint8_t remote_address;
	uint8_t AT[3];			//2 bytes
	uint8_t data[20];		//AT data
	uint8_t data_length;	
};
enum delivery_status_type {
	DELIVERY_STATUS_SUCCESS				=	0X00,	
	DELIVERY_STATUS_MAC_ACK_FAIL		=	0X01,		
	DELIVERY_STATUS_CCA					=	0X02,	
	DELIVERY_STATUS_PKG_PURGED			=	0X03,
	DELIVERY_STATUS_PHY					=	0X04,
	DELIVERY_STATUS_NO_BUFFERRS			=	0X18,
	DELIVERY_STATUS_NETWORK_NO_ACK		=	0X21,
	DELIVERY_STATUS_NO_NETWORK_JOINED	=	0X22,
	DELIVERY_STATUS_SELF_ADDRESS		=	0x23,
	DELIVERY_STATUS_ADD_NOT_FOUND		=	0x24,
	DELIVERY_STATUS_ROUTE_NOT_FOUND		=	0x25,
	DELIVERY_STATUS_BROAD_NOT_RELAY		=	0x26
};

enum package_type {
	RECEIVED_PACKAGE		=	0X90,			
	TRANSMIT_REQUEST		=	0X10,	
	DEVICE_REQUEST			=	0XB9,			//N�o Implementado
	DEVICE_RESPONSE_STATUS	=	0XBA,			//N�o Implementado
	DEVICE_RESPONSE			=	0X2A,			//N�o Implementado
	TRANSMIT_STATUS			=	0X8B
};
typedef enum package_type package_type_genare_t;

struct xbee_msg 
{
	uint8_t type;
	uint8_t data_lenght;
	uint16_t adress_sender16;
	uint8_t data[100];
	uint64_t adress_sender64;
};
uint8_t xbee_send_msg(uint64_t address,uint8_t *msg, uint16_t msg_lenght);
uint8_t xbee_send_AT(struct xbee_at_msg *msg);
uint8_t xbee_send_str(uint64_t address,const char msg[]);
void xbee_config(uint32_t baud);
enum status_code xbee_network_status(void);
void xbee_reset(void);

//Cabe�alho da mensagem para o xbee, formato para envio tipo 10 no byte 4
uint8_t MSG[]={0x7E,0x00,0x0E,0x10,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0xFF,0xFE,0x00,0x00};
//Prototipo das fun�oes
enum status_code xbee_read(uint8_t *data);

uint8_t flag_header=0,flag_pck=0, flag_buffer=1;
uint16_t read_flag=0,write_flag=0;
static uint16_t data_serial=0;
uint16_t wait_time;

uint8_t frame_id_at=1;
uint8_t xbee_send_AT(struct xbee_at_msg *msg){
	uint8_t data[8+msg->data_length];
	data[0]=0x7E;
	data[1]=0;
	data[2]=4+msg->data_length;
	data[3]=0x08;
	data[4]=frame_id_at;
	if(frame_id_at==0){
		frame_id_at=1;
	}
	data[5]=msg->AT[0];
	data[6]=msg->AT[1];
	for(int i=0;i<msg->data_length;i++)
		data[7+i]=msg->data[i];
	uint8_t sum_pos=7+msg->data_length;
	data[sum_pos]=0;
	for(int i=3;i<(7+msg->data_length);i++){
		data[sum_pos]+=data[i];
	}
	data[sum_pos]=0xFF-data[sum_pos];
	Serial.write((uint8_t*)data,8+msg->data_length);
	return frame_id_at++;
}

uint8_t xbee_send_str(uint64_t address,const char msg[]){
	uint16_t msg_l=0;
	while((msg[msg_l++]!='\0'));
	return xbee_send_msg(address,(uint8_t*)msg,msg_l);
}
//retorna o indentificador da mensagem
uint8_t xbee_send_msg(uint64_t address,uint8_t msg[], uint16_t msg_lenght){
	uint8_t *address64=(uint8_t*)&address;
	MSG[4]++;
	if(MSG[4]==0)	//O indentificador 0 n�o retorna status
		MSG[4]=1;
	uint8_t sum=(uint8_t)0x0D+MSG[4];
	for(int i=0;i<8;i++){
		MSG[5+i]=address64[7-i];
		sum+=MSG[5+i];
	}
	uint16_t pkg_lenght=msg_lenght+0x0E;
	MSG[2]=(uint8_t)pkg_lenght;
	MSG[1]=(uint8_t)((pkg_lenght&0xFF00)>>8);
	for(int i=0;i<msg_lenght;i++){
		sum+=msg[i];
	}
	sum=(uint8_t)(0xFF-sum);
	uint16_t sum16=sum;
	Serial.write((uint8_t*)MSG,17);
	Serial.write((uint8_t*)msg,msg_lenght);
	Serial.write(sum16);
	return MSG[4];
}
void xbee_config(int baud){
  Serial.begin(baud);
  wait_time=50000;
}

uint16_t size_pkg;

enum status_code xbee_read_msg(struct xbee_msg *mensagem){
	uint8_t data;
	if(Serial.available()>6){
		//For debug Time
		//port_pin_toggle_output_level(LED_0_PIN);
		data=Serial.read();
		if(data==((uint8_t)0x7E)){		//Byte de Inicio do pacote do xbee
			data=Serial.read();
			uint16_t lengthz=data;
			data=Serial.read();
			lengthz=(lengthz<<8)+data-1;
			data=Serial.read();
			uint8_t check_sum,check_sum_local=0;
			mensagem->type=data;
			int i=0,time_out=0,header=11;
			check_sum_local+=data;
			uint8_t  ok_data;
			//Captura a mensagem do tipo RECEIVED PACKAGE
			if(mensagem->type==RECEIVED_PACKAGE){
				mensagem->data_lenght=lengthz-header;
				//ignora o warning
				//#pragma GCC diagnostic ignored  "-Wincompatible-pointer-types"
				uint8_t *address64=(uint8_t*)&mensagem->adress_sender64;
				uint8_t *address16=(uint8_t*)&mensagem->adress_sender16;
				for(;i<=lengthz;i++){
					ok_data=1;
					time_out=0;
					while(ok_data){
						if((time_out++)==wait_time)
							return STATUS_ERR_TIMEOUT;
						else{
							if(Serial.available()){
								data=Serial.read();
								if(i<8){
									address64[7-i]=(uint8_t)data;
								}else if(i<10){
									address16[9-i]=(uint8_t)data;
								}else if(i==lengthz){
									check_sum=data;
									if((0xff-check_sum_local)==check_sum){
										//For debug Time
										//port_pin_toggle_output_level(LED_0_PIN);
										return STATUS_OK;
									}
								}else  if((i>=header)){
									mensagem->data[i-header]=(uint8_t)data;
								}
								check_sum_local+=data;
								ok_data=0;
							}
						}
					}
				}
			}else{
				mensagem->data_lenght=lengthz;
				for(i=0;i<=lengthz;i++){
					ok_data=1;
					time_out=0;
					while(ok_data){
						if((time_out++)==wait_time){
							return STATUS_ERR_TIMEOUT;
						}else{
							if(Serial.available()){
                data=Serial.read();
								if(i==lengthz){
									check_sum=data;
									if((0xff-check_sum_local)==check_sum)
									return STATUS_OK;
								}
								mensagem->data[i]=(uint8_t)data;
								check_sum_local+=data;
								ok_data=0;
							}
						}
					}
				}
				
			}
			return STATUS_ERR_BAD_DATA;
		}
	}
	return STATUS_ERR_NOT_FOUND;
}
void xbee_reset(){
	struct xbee_at_msg	new_msg;
	new_msg.AT[0]='F';new_msg.AT[1]='R';
	new_msg.destination=0;
	new_msg.data_length=0;
	xbee_send_AT(&new_msg);	
}
