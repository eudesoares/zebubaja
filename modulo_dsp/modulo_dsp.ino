/*************************************************************************************************************
                                                       INCLUDES
 ************************************************************************************************************/
#include "config.h"                                                                                  // Arquivo da configuração das variáveis iniciais
#include "glcd.h"                                                                                            // Biblioteca do display GLCD
#include "fonts/allFonts.h"                                                                                  // Carrega fontes para o GLCD
#include "bitmaps/allBitmaps.h"                                                                              // Carrega bitmaps para o GLCD                                                                                    
#include <Wire.h> // Biblioteca Wire 
//#include <DS1307.h>   // Biblioteca do DS1307
#include <EEPROM.h>


/*************************************************************************************************************
                                           Variaveis de controle do display
 ************************************************************************************************************/
int  j = 10, on = 0;;                                                                         // velocidade instantanea km/h
bool test = 0;
int motor_temp_dsp = 0;                                                                                      // Temperatura do motor no display
int cvt_temp_dsp = 0;                                                                                        // Temperatura do CVT no display
bool tensao_dsp = 0, comb_nivel_dsp = 0, cvt_temp_erro = 0, motor_temp_erro = 0, io_com_state = 0;           // Representa o estado do simbolo no display (tela principal)
int clock[7], set_data = 0, read_state = 0;                                                                  // Variavel que recebe os dados de data e hora do ds1307
unsigned long int time, pisca, error_msn_time, erro_buzz_time = -1, erro_bar_time, B1_time, B2_time, B3_time; // Controla o pisca do timer e imagens
bool b1_set = 0, b2_set = 0, b3_set = 0, ponteiro = 1, pisca_state = 0, erro_mensagem_status = 0, erro_mensagem_clear = 0; // Situação do ponteiro do timer e simbolos de erro (visivel e invisivel)
int check_pos[] = {16, 28, 40, 52, 16}, c_pos = 4;
int command[] = {10, 30, 40, 50, 20}; //10=desligado; 20=aceleracao, 30=suspencion, 40=tracao, 50=enduro
int cvt_temp_dsp2 = -1;
int motor_temp_dsp2 = -1;
int di0 = 0, di1 = 0 - 1;
int rec_state2 = -1;
int velo_ant = -1;
char page =0;
/*************************************************************************************************************
                                           Controle de fonte do display
 ************************************************************************************************************/
gText t1;
gText t2;

/*************************************************************************************************************
                                           Setup
 ************************************************************************************************************/
#include "xbee.h"
#include "communication.h"
#include "screens.h"

void setup() {
  //EEPROM.write(0, 4);
  c_pos = EEPROM.read(0);
  //DS1307.setDate(1,1,1,1,20,30,1);
  xbee_config(9600);                                                                                    // Define velocidade de comunicação
  //    DS1307.begin();                                                                                          // configura os pinos para o ds1307
  t1.SelectFont(System5x7);                                                                                // Define fonte
  t2.SelectFont(fixednums15x31);                                                                           // Define fonte
  GLCD.Init();                                                                                             // initialize the library, non inverted writes pixels onto a clear screen
//  GLCD.DrawBitmap(ZebuBaja2, 0, 0);                                                                        //draw the bitmap at the given x,y position
//  while (millis() < 700) {
//    if (leituraMsgCar(&carData)) {                                                                                        // Se a função retornar que: "data[dados] == data[dados1]"
//       GLCD_Main_Update();                                                                                       // Atualiza os dados recebidos pela serial
//    }
//  }
//  GLCD.ClearScreen();
//  GLCD.DrawBitmap(uftm3, 0, 0);                                                                              //draw the bitmap at the given x,y position
//  while (millis() < 1400) {
//    if (leituraMsgCar(&carData)) {                                                                                        // Se a função retornar que: "data[dados] == data[dados1]"
//      GLCD_Main_Update();                                                                                        // Atualiza os dados recebidos pela serial
//    }
//  }
  GLCD.ClearScreen();
  GLCD_Main();                                                                                             // design da Tela principal
  pinMode(2, INPUT_PULLUP);                                                                                       // botão do volante
  pinMode(13, INPUT_PULLUP);                                                                                       // botão do volante
  pinMode(buzz, OUTPUT);                                                                                   // buzz (na placa do display)
//  digitalWrite(buzz, LOW);                                                                                 // inicia o buzz desativado
}

/*************************************************************************************************************
                                           Loop Principal
 ************************************************************************************************************/
void loop() {
  if(leituraMsgCar(&carData)){             //Le o pacte de dados
    sendMsgCar(&carData);
  }
//  data_network car;
//  if (leituraMsgCar(&car)==1) {                      // Se receber corretamente os dados pela Serial
//    //sendMsgCar(&carData);
//    xbee_send_str(0xffff,"Teste");
//    //GLCD_Main_Update();                             // Atualiza os dados do display
//  }
//  read_state = button_read();
//  if (read_state == 305) {                                                                            // Se os dois botões forem precionsador por 1.5seg muda a tela
//    b3_set = 1;
//    page++;
//    GLCD.ClearScreen();
//    if (page == 3) {
//      page = 0;
//    }
//    if (page == 0) {
//      GLCD_Main();
//      cvt_temp_dsp2 = -1;
//      motor_temp_dsp2 = -1;
//      di0 = 0, di1 = 0 - 1;
//      rec_state2 = -1;
//      velo_ant = -1;
//    }
//    if (page == 1) {
//      GLCD_2_tela();
//    }
//    if (page == 2) {
//      GLCD.DrawBitmap(ZebuBaja2, 0, 0); //draw the bitmap at the given x,y position
//    }
//  }


//  if (page == 0) {                                                                                           // TELA INICIAL DO DISPLAY
//    if (!error_mensagem()) {                                                                                 // Se não tiver mensagem de erro do Combustível,
//      if (leitura) {
//        GLCD_Main_Update();                                                                                    // Mostra e atualiza as informações da tela principal
//        velo_update();
//      }
//      if (((millis() - pisca) > erro_pisca_on) && (pisca_state == 1)) {                                      // Verifica se há mensagens de erro
//        GLCD_error_update();                                                                                 // Imprimi no display o erro
//      }
//      if (((millis() - pisca) > erro_pisca_off) && (pisca_state == 0)) {                                     // Verifica se há mensagens de erro
//        GLCD_error_update();                                                                                 // Imprimi no display o erro
//      }
//      //        time_update();
//    }
//  }
//  if (page == 1) {                                                                                           // SEGUNDA TELA DO DISPLAY
//    GLCD_Info_update();
//    velo_update();
//  }
}


/*************************************************************************************************************
               VERIFICA OS VALORES RECEBIDOS PARA ATRIBUIR OU LIMPAR AS FLAGS DE FALHA
 ************************************************************************************************************/ 
void fail_check() {
  ////////////////////////////  Verifica se é necessário acionar a flag de falha do CVR  /////////////////////////////////////
  if (carData.t_motor > motor_temp_max) {                                   // verifica se o motor atingiu a temperatura máxima
    motor_temp_erro = 1;
  }else {
    motor_temp_erro = 0;
  }
  ////////////////////////////  Verifica se é necessário acionar a flag de falha do CVR  /////////////////////////////////////
  if (carData.t_cvt > cvt_temp_max) {                                  // verifica se o cvt atingiu a temperatura máxima
    cvt_temp_erro = 1;
  }else {
    cvt_temp_erro = 0;
  }
  ////////////////////////  Verifica se é necessário acionar a flag de nível de combustível  /////////////////////////////////
  if (carData.combustivel > 10) {                                      // verifica se o combustível está abaixo do setpoint
    comb_nivel_dsp = 0;
  } else {
    comb_nivel_dsp = 1;
  }
}
