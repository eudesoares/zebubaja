#include "xbee.h"
void setup() {
  // put your setup code here, to run once:
  xbee_config(9600);
  pinMode(13,OUTPUT);
}

void loop() {
  leitura_dados_display();
}
void leitura_dados_display() {
  XbeeMsg mensagem;
  uint8_t val2[10];
  mensagem.data=(uint8_t*)val2;  
  if (xbee_read_msg(&mensagem)==STATUS_OK) {
      if(mensagem.type==RECEIVED_PACKAGE){
        if (val2[0] == 'D') {
          digitalWrite(12, HIGH);
        }
        else if (val2[0] == 'L') {
          digitalWrite(12, LOW);
        }
    }
  }
}
