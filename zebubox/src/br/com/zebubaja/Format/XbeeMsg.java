/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zebubaja.Format;

import br.com.zebubaja.Enum.ComandType;
import br.com.zebubaja.Enum.MsgXbeeType;
import br.com.zebubaja.bean.TransmitStatusMsg;
import br.com.zebubaja.bean.XbeeMsgBean;
import br.com.zebubaja.serial.Conexao;
import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;


/**
 * Classe para formatação de mensagens do Xbee e envio de mensagens na rede dos dosadores
 * @author Eude Soares Santana <a href="jin.ss.ptu@gmail.com">Eude Soares</a>
 */

public class XbeeMsg implements Observer{
    public Conexao conexao;
    public char data[];
    private byte control=0;
    private int tx_status;
    private int id_msg=0;
    /**
     * Formata a mensagem do xbee e dependendo do pacote recebido ({@link br.com.agente.Enum.MsgXbeeType#RECEIVED_PACKAGE}), formata a mensagem da rede
     * chamando o método {@link br.com.agente.MsgFormat.NetworkMsg#format(char[])};
     * @return retorna o objeto com os dados da mensagem do xbee e da rede.
     * @see br.com.agente.MsgFormat.NetworkMsg#format(char[]) 
     * @param data array recebido pela Serial do xbee
     */
    public XbeeMsgBean format(char data[]){
        XbeeMsgBean msg=new XbeeMsgBean();
        msg.setXbee_msg_type(MsgXbeeType.findType((int)data[1])); 
        short add16;
        switch(msg.getXbee_msg_type()){
            case RECEIVED_PACKAGE:
                long address=0;
                for(int i=2;i<10;i++){
                    address=address<<8;
                    address=address|(long)data[i];
                }
                msg.setAddress64(address);
                add16=(short) (data[10]*256);
                add16+=(short) data[11];
                msg.setAddress16(add16);
                CarDataFmt car_fmt=new CarDataFmt();
                msg.setCarData(car_fmt.format(data));
                break;
            case TRANSMIT_STATUS:
                add16=(short) (data[3]*256);
                add16+=(short) data[4];
                msg.setAddress16(add16);
                TransmitStatusMsg transmitStatus=new TransmitStatusMsg();
                transmitStatus.setTransmitRetryCount((byte) data[5]);
                transmitStatus.setDeliveryStatus((byte) data[6]);
                transmitStatus.setDiscoveryStatus((byte) data[7]);
                transmitStatus.setFrameID((byte) data[2]);
                msg.setTransmitStatus(transmitStatus);
                break;
        }
        return msg;
    }
    /**
     * Formata a data-hora para o formato utilizado na rede
     * @param date Data a ser formatada
     * @return Retorna valor long com o valor da data em 32bits unsigned
     */
    public long dataFormat(Calendar date){
        long date_time=0;
        date_time|=((date.get(Calendar.DAY_OF_MONTH))&0b11111);
        date_time=date_time<<4;
        date_time|=((date.get(Calendar.MONTH)+1)&0b1111);
        date_time=date_time<<6;
        date_time|=(date.get(Calendar.YEAR)-2000)&0b111111;
        date_time=date_time<<5;
        date_time|=(date.get(Calendar.HOUR_OF_DAY)&0b11111);
        date_time=date_time<<6;
        date_time|=(date.get(Calendar.MINUTE) &0b111111);
        date_time=date_time<<6;
        date_time|=(date.get(Calendar.SECOND)&0b111111);
        return date_time;  
    }
    
    /**
     * Envia uma mensagem para o destinatario (MAC addres) na rede.
     * {@link br.com.agente.Bean.NetworkMsgBean};
     * @param address Endereço do dosador de destino
     * @param type
     * @return Retorna o identificador da mensagem para contorle do transmit_status
     * @throws InterruptedException .
     */
    public byte sendNetworkCmd(long address,ComandType type) throws InterruptedException{
        byte char_msg[];
        byte checksum;
        int start_msg=17;//inicio da mensagem
        char_msg=new byte[19];
        char_msg[2]=15;//Tamanho
        char_msg[0]=0x7e;
        char_msg[1]=0;
        char_msg[3]=(byte) MsgXbeeType.TRANSMIT_REQUEST.getValor();
        if(control==0)
            control=1;
        char_msg[4]=control++;
        for(int i=7;i>=0;i--){
            char_msg[5+i]=(byte)address;
            address=address>>8;
        }
        char_msg[13]=(byte) 0xFF;
        char_msg[14]=(byte) 0xFe;
        char_msg[15]=0x00;      //BROADCAST RADIUS  
        char_msg[16]=0x01;      //OPTION
        //Inicio dos dados
        char_msg[start_msg]=(byte) type.getValor();
        checksum=0;
        for(int i=0; i<=char_msg[2];i++){
            checksum+=char_msg[i+3];
        }
        checksum=(byte) (0xFF-checksum);
        char_msg[char_msg.length-1]=checksum;
        conexao.Enviar(char_msg);
        return char_msg[4];
    }
    /**
     * Envia a mensagem e aguarda a resposta do Transmit status
     * @param dosador Endereço do dosador de destino
     * @param type
     * @return  Retorna o delivery_status da resposta da transmissão
     * @throws java.lang.InterruptedException .
     */
    public int sendNetworkCmdWait(long dosador, ComandType type) throws InterruptedException {
        //System.err.println("  " + Long.toHexString(dosador)+"  ");
        conexao.addObserver(this);
        id_msg=sendNetworkCmd(dosador, type);
        long t=System.currentTimeMillis();
        setTx_status((int)0xff);
        while((getTx_status()==0xFF)&&(System.currentTimeMillis()-t)<1500){
        }
        conexao.deleteObserver(this);
        return getTx_status();
    }
    /**
     * Observador da serial para verificação do transmit status
     * @param o Retorna o objeto observado
     * @param dados Objeto de dados do observado
     */
    @Override
    public void update(Observable o, Object dados) {
        XbeeMsg xbee=new XbeeMsg();
        char mensagem[]=(char[]) dados;
        XbeeMsgBean msg;
        msg = xbee.format(mensagem);
        if(msg.getXbee_msg_type()==MsgXbeeType.TRANSMIT_STATUS){
            if(id_msg==msg.getTransmitStatus().getFrameID()){
                setTx_status(msg.getTransmitStatus().getDeliveryStatus());
                //System.err.println(msg.getTransmitStatus().getDeliveryStatus());
            }
        }
    }
    /**
      * Pega o valor de tx_status no contexto sincronizado
      * @return the tx_status
      */
    public synchronized int getTx_status() {
        return tx_status;
    }
    /**
     * Atribui valor a tx_status no contexto sincronizado
     * @param tx_status the val to set
     */
    public synchronized void setTx_status(int tx_status) {
        this.tx_status = tx_status;
}
}
