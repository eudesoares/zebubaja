/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

/**
 *
 * @author Spider
 */
public class SerialComLeitura implements Runnable, SerialPortEventListener {
    private boolean Leitura;
    private boolean IDPortaOK;
    private boolean PortaOK;
    private InputStream entrada;
    public int nodeBytes;
    private final int baudrate;
    private final String Porta;
    private final int timeout;
    private Thread threadLeitura;
    private boolean Escrita;
    protected String[] portas;
    protected Enumeration listaDePortas;
    public String Dadoslidos;
    private CommPortIdentifier cp;
    private SerialPort porta;
    protected String peso;
    public SerialComLeitura( String p , int b , int t ){
        this.Porta = p;
        this.baudrate = b;
        this.timeout = t;
    } 
    public void HabilitarEscrita(){
        Escrita = true;
        Leitura = false;

    }
    public void HabilitarLeitura(){

            Escrita = false;
            Leitura = true;

    }
    public String[] ObterPortas(){
            return portas;
    }
    public void AbrirPorta(){
            try {
                porta = (SerialPort)cp.open("SerialComLeitura", timeout);
                PortaOK = true;
                //configurar parâmetros
                porta.setSerialPortParams(baudrate,
                porta.DATABITS_8,
                porta.STOPBITS_1,
                porta.PARITY_NONE);
                porta.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);
            }catch(PortInUseException | UnsupportedCommOperationException e){
                PortaOK = false;
                System.out.println("Erro abrindo comunicação: " + e);
                System.exit(1);
            }
    }
    public void LerDados(){
            if (Escrita == false){
                try {
                    entrada = porta.getInputStream();
                } catch (Exception e) {
                    System.out.println("Erro de stream: " + e);
                    System.exit(1);
                }
                try {
                    porta.addEventListener((SerialPortEventListener) this);
                } catch (Exception e) {
                    System.out.println("Erro de listener:"  + e);
                    System.exit(1);
                }
                porta.notifyOnDataAvailable(true);
                try {
                    threadLeitura = new Thread((Runnable) this);
                    threadLeitura.start();
                   run();
                } catch (Exception e) {
                    System.out.println("Erro de Thred: " + e);
                }
            }
    }
    public void ObterIdDaPorta(){
            try {
                cp = CommPortIdentifier.getPortIdentifier(Porta);
                if ( cp == null ) {

                    System.out.println("Erro na porta");
                    IDPortaOK = false;
                    System.exit(1);
                }
                IDPortaOK = true;
            } catch (Exception e) {
                System.out.println("Erro obtendo ID da porta: " + e);
                IDPortaOK = false;
                System.exit(1);
            }
    }
    public void FecharCom(){

            try {

                porta.close();

            } catch (Exception e) {

                System.out.println("Erro fechando porta: " + e);

                System.exit(0);

            }

}
    @Override
    public void serialEvent(SerialPortEvent ev){       
                StringBuffer bufferLeitura = new StringBuffer();
                int novoDado = 0;
                switch (ev.getEventType()) {
                    case SerialPortEvent.BI:
                    case SerialPortEvent.OE:
                    case SerialPortEvent.FE:
                    case SerialPortEvent.PE:
                    case SerialPortEvent.CD:
                    case SerialPortEvent.CTS:
                    case SerialPortEvent.DSR:
                    case SerialPortEvent.RI:
                    case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                    break;
                    case SerialPortEvent.DATA_AVAILABLE:
                        while(novoDado != -1){
                            try{
                                novoDado = entrada.read();
                                if(novoDado == -1){
                                    break;
                                }
                                System.out.println((char)novoDado);
                                if('\r' == (char)novoDado){
                                    bufferLeitura.append('\n');
                                }else{
                                    bufferLeitura.append((char)novoDado);
                                }
                            }catch(IOException ioe){
                                System.out.println("Erro de leitura serial: " + ioe);
                            }
                        }
                        setPeso(new String(bufferLeitura));
                        System.out.println(getPeso());

                    break;

                }

        }
    public void setPeso(String peso){
            this.peso = peso;
    }
    public String getPeso(){
            return peso;
    }
    @Override
    public void run(){
        try {
            Thread.sleep(5);
        } catch (Exception e) {
            System.out.println("Erro de Thred: " + e);
        }
}
    
}
