typedef struct data_network{
  char velocidade;     //8bits  Variação de 0 a 255   Km/h
  char inicio;         //Inicio do pacote, byte 0x7E
  short rpm;           //16bits Variação de 0 a 65536 rpm
  short t_cvt;         //16bits 0 a 1024
  short t_motor;       //16bits 0 a 1024
  short tensao;        //16bits 0 a 1024
  short combustivel;   //16bits 0 a 1024
  uint32_t distancia;  //32bits 0 a 2^32 em metros
  uint32_t longitude;  //32 bits longitude
  uint32_t latitude;   //32 bits latitude
}data_network;
static data_network carData;   //struct com os dados do veículo
uint64_t addressBox=0xFFFF;         //Endereço do xbee do box
uint64_t addressPad=0xFFFF;         //Endereço do xbee do Pad
