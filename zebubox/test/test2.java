/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Spider
 */
public class test2 {
public static void main(String[] args){     
        //Iniciando leitura serial
        SerialComLeitura leitura = new SerialComLeitura("COM5", 9600, 0);
        leitura.HabilitarLeitura();
        leitura.ObterIdDaPorta();
        leitura.AbrirPorta();
        leitura.LerDados();
        //Controle de tempo da leitura aberta na serial
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            System.out.println("Erro na Thread: " + ex);
        }
        leitura.FecharCom();
    }
}