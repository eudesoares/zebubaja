//////////////////////////////////////////////////////////////////CONFIGURAÇÃO DE PINOS///////////////////////////////////////////////////////////////////////////////////////
int const buzz=13;                                //pino de acionamento do buzz

////////////////////////////////////////////////////////////VARIAVEIS DE TEMPO RELACIONADAS A ERRO////////////////////////////////////////////////////////////////////////////
int const erro_msn_duration = 3000;               //Tempo de duraçao da mensagem de tela cheia em milisegundos
int const erro_msn_pisca_on = 800 ;               //Tempo de aparição do simbolo de erro em tela cheia antes de apagar no estado piscante em milisegundos
int const erro_msn_pisca_off = 100 ;              //Tempo de desaparecimento do simbolo de erro em tela cheia antes de aparecer no estado piscante em milisegundos
int const erro_pisca_on = 300;                    //Tempo de aparição do simbolo de erro na tela principal antes de apagar no estado piscante em milisegundos
int const erro_pisca_off = 100;                   //Tempo de desaparecimento do simbolo de erro na tela principal antes de aparecer no estado piscante em milisegundos
int const erro_buzz = 100;                        //Tempo de sinalização do bazzer diante de um novo erro

/////////////////////////////////////////////////////////////////// TEMPERATURA DO CVT //////////////////////////////////////////////////////////////////////////////////////
int const cvt_temp_max = 100;                     //Temperatura máxima do cvt para acionamento da mensagem de alta temperatura em Cº
int const cvt_min_range=0;                        //Temperatura minima do motor a mostra no display na barra de temperatura Cº
int const cvt_max_range=150;                      //Temperatura minima do motor a mostra no display na barra de temperatura Cº  obs: max > min

////////////////////////////////////////////////////////////////// TEMPERATURA DO MOTOR /////////////////////////////////////////////////////////////////////////////////////
int const motor_temp_max = 100;                   //Temperatura máxima do mootor para acionamento da mensagem de alta temperatura em Cº
int const motor_min_range=0;                      //Temperatura minima do motor a mostra no display na barra de temperatura Cº
int const motor_max_range=150;                    //Temperatura minima do motor a mostra no display na barra de temperatura Cº  obs: max > min

/////////////////////////////////////////////////////////////// CONSTANTES DE TEMPO DOS BOTOES //////////////////////////////////////////////////////////////////////////////
int const time_setup= 3000;

/////////////////////////////////////////////////////////////////Variáveis e estruturas globais//////////////////////////////////////////////////////////////////////////////


const int simbolX[5] = {12, 30, 48, 66, 84}, simbolY = 52;                                                   // Posições das imagens de erro no display

typedef struct data_network{
  char velocidade;     //8bits  Variação de 0 a 255   Km/h
  char v_max;
  char inicio;         //Inicio do pacote, byte 0x7E
  short rpm;           //16bits Variação de 0 a 65536 rpm
  short t_cvt;         //16bits 0 a 1024
  short t_motor;       //16bits 0 a 1024
  short tensao;        //16bits 0 a 1024
  short combustivel;   //16bits 0 a 1024
  uint32_t distancia;  //32bits 0 a 2^32 em metros
  uint32_t longitude;  //32 bits longitude
  uint32_t latitude;   //32 bits latitude
}data_network;
static data_network carData;        //struct com os dados do veículo
uint64_t addressBox=0xFFFF;         //Endereço do xbee do box
uint64_t addressPad=0xFFFF;         //Endereço do xbee do Pad
