/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zebubaja.GpsRoute;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Spider
 */
public class GpsData {

    ArrayList<Integer> longitude = new ArrayList<>();
    ArrayList<Integer> latitude = new ArrayList<>();

    public GpsData() throws FileNotFoundException, IOException {
        Scanner ler = new Scanner(System.in);
        try (FileReader arq = new FileReader("LOG.txt")) {
            BufferedReader lerArq = new BufferedReader(arq);
            String linha = lerArq.readLine(); // lê a primeira linha
            String[] valor;
            
            while (linha != null) {
                valor = linha.split(";",2);
                longitude.add(Integer.valueOf(valor[0]));
                latitude.add(Integer.valueOf(valor[1]));
                linha = lerArq.readLine(); // lê da segunda até a última linha
            }
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
        }
        System.out.println();
    }
}
