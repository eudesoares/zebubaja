/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zebubaja.bean;

/**
 *
 * @author Spider
 */
public class RecProfile {

    private String nome;
    private String diretorio;
    private boolean m_temp = false;
    private boolean c_temp = false;
    private boolean velocidade = false;
    private boolean rpm = false;
    private boolean tensao = false;
    private boolean v_max = false;
    private boolean longitude = false;
    private boolean latitude = false;
    private int selected;
    private String estados;
    private  String file_name;
    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the diretorio
     */
    public String getDiretorio() {
        return diretorio;
    }

    /**
     * @param diretorio the diretorio to set
     */
    public void setDiretorio(String diretorio) {
        this.diretorio = diretorio;
    }

    /**
     * @return the m_temp
     */
    public boolean isM_temp() {
        return m_temp;
    }

    /**
     * @param m_temp the m_temp to set
     */
    public void setM_temp(boolean m_temp) {
        this.m_temp = m_temp;
    }

    /**
     * @return the c_temp
     */
    public boolean isC_temp() {
        return c_temp;
    }

    /**
     * @param c_temp the c_temp to set
     */
    public void setC_temp(boolean c_temp) {
        this.c_temp = c_temp;
    }

    /**
     * @return the velocidade
     */
    public boolean isVelocidade() {
        return velocidade;
    }

    /**
     * @param velocidade the velocidade to set
     */
    public void setVelocidade(boolean velocidade) {
        this.velocidade = velocidade;
    }

    /**
     * @return the rpm
     */
    public boolean isRpm() {
        return rpm;
    }

    /**
     * @param rpm the rpm to set
     */
    public void setRpm(boolean rpm) {
        this.rpm = rpm;
    }

    /**
     * @return the tensao
     */
    public boolean isTensao() {
        return tensao;
    }

    /**
     * @param tensao the tensao to set
     */
    public void setTensao(boolean tensao) {
        this.tensao = tensao;
    }

    /**
     * @return the v_max
     */
    public boolean isV_max() {
        return v_max;
    }

    /**
     * @param v_max the v_max to set
     */
    public void setV_max(boolean v_max) {
        this.v_max = v_max;
    }

    /**
     * @return the longitude
     */
    public boolean isLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(boolean longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the latitude
     */
    public boolean isLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(boolean latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the selected
     */
    public int getSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(int selected) {
        this.selected = selected;
    }

    /**
     * @return the estados
     */
    public String getEstados() {
        if (m_temp) {
            estados = ";1";
        } else {
            estados = ";0";
        }
        if (rpm) {
            estados += ";1";
        } else {
            estados += ";0";
        }
        if (c_temp) {
            estados += ";1";
        } else {
            estados += ";0";
        }
        if (v_max) {
            estados += ";1";
        } else {
            estados += ";0";
        }
        if (velocidade) {
            estados += ";1";
        } else {
            estados += ";0";
        }
        if (latitude) {
            estados += ";1";
        } else {
            estados += ";0";
        }
        if (longitude) {
            estados += ";1";
        } else {
            estados += ";0";
        }
        if (tensao) {
            estados += ";1";
        } else {
            estados += ";0";
        }
        return estados;
    }

    /**
     * @return the file_name
     */
    public String getFile_name() {
        return file_name;
    }

    /**
     * @param file_name the file_name to set
     */
    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

}
