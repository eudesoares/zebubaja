#include "xbee.h"
#include "defines.h"
#include "comunicacao.h"
#include "glcd.h"                                                                                            // Biblioteca do display GLCD
#include "fonts/allFonts.h"                                                                                // Carrega fontes para o GLCD
#include "bitmaps/allBitmaps.h"  
gText t1;
gText t2;
void setup() {
  t1.SelectFont(System5x7);                                                                                // Define fonte
  t2.SelectFont(fixednums15x31);                                                                           // Define fonte
  GLCD.Init(); 
  GLCD.ClearScreen();
  GLCD_Main();  
  xbee_config(9600);      //Inicializa a porta serial do xbee
}
void loop() {
if(leituraMsgCar(&carData)){             //Le o pacte de dados
    sendMsgCar(&carData);
    velo_update();
  }
}
int velo_ant=0;
void velo_update() {
  t2.CursorToXY(48, 21);
  char data[20];
  sprintf(data,"%d",(int)carData.velocidade);
  t2.print(data);
}
void GLCD_Main() {
  ////////////////////Ajusta o mostrador de  Temperatura do motor //////////////////////////////////////////
  GLCD.DrawBitmap(motor_temp_simbol, 0, 0);       // Desenha o simbolo da temperatura do motor sobre a barra
  GLCD.DrawRect(0, 11, 10, 52);                   // Desenha o retangulo externo (vazio) - temp bar
  //////////////////////Ajusta o mostrador de  Temperatura do CVT //////////////////////////////////////////
  GLCD.DrawBitmap(cvt_temp_simbol, 117, 0);       // Desenha o simbolo do motor
  GLCD.DrawRect(117, 11, 10, 52);                 // Desenha o retangulo externo (vazio) - temp bar
  ////////////////////////  Ajusta a barra superior do display /////////////////////////////////////////////
  GLCD.DrawRoundRect(18, 0, 92, 10, 4);           // Desenha o retangulo externo, vazio
  t1.CursorToXY(66, 2);                           // Posiçãoo do divisor do retangulo
  t1.Printf_P(PSTR("|"));                         // Divisor do retangulo
  t1.CursorToXY(22, 2);                           // posição£o no display
  t1.Printf_P(PSTR("     Km"));                   // desenha o formato e unidade
  /////////////////////Edita o mostrador de Velocidade Instantanea//////////////////////////////////////////
  t2.CursorToXY(48, 21);
  t2.Printf_P(PSTR("%02d"), carData.velocidade);         // escreve no display a velocidade
  t1.CursorToXY(80, 42);
  t1.Printf_P(PSTR("Km/h"));
}


