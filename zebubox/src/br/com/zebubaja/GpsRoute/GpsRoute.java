package br.com.zebubaja.GpsRoute;

import br.com.zebubaja.bean.Gpsplot;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hansolo
 */
public final class GpsRoute extends javax.swing.JComponent {
    Gpsplot dados=new Gpsplot();
    private int longitude;
    private int latitude;
    private float min = 0;
    private float max = 3800;

    public enum luminosity {
        LIGHT, DARK
    };
    public luminosity brightness = luminosity.DARK;
    private java.awt.image.BufferedImage RouteImage = createCar();
    private final java.awt.image.BufferedImage CarImage = createCar();
    private final double offsetX = -832;
    private final double offsetY = 0;
    private final double value = 0;
    private final double factor = offsetX / (min - max);
    java.awt.geom.AffineTransform oldTransform;
    //private final java.awt.image.BufferedImage ponto = createPonto();
    int tamanho = 0;
    public GpsRoute() throws IOException {
        super();
        init();
    }

    private void init() throws IOException {

    }

    @Override
    protected void paintComponent(java.awt.Graphics g) {
        java.awt.Graphics2D g2 = (java.awt.Graphics2D) g;
        if((dados.getGraficoSizeX()>dados.getSizeX())&&!(dados.getGraficoSizeY()>dados.getSizeY()))
            g2.drawImage(RouteImage,0, +(dados.getSizeY()-dados.getGraficoSizeY())/2, null); 
        else if(!(dados.getGraficoSizeX()>dados.getSizeX())&&(dados.getGraficoSizeY()>dados.getSizeY()))
            g2.drawImage(RouteImage,(dados.getSizeX()-dados.getGraficoSizeX())/2, 0, null); 
        else if((dados.getGraficoSizeX()>dados.getSizeX())&&(dados.getGraficoSizeY()>dados.getSizeY()))
            g2.drawImage(RouteImage,0, 0, null); 
        else
            g2.drawImage(RouteImage, (dados.getSizeX()-dados.getGraficoSizeX())/2, (dados.getSizeY()-dados.getGraficoSizeY())/2, null);

        //g2.drawImage(CarImage, longitude - 20, -latitude - 30, null);
    }

    public void createRoute(Gpsplot dados) throws IOException {
        this.dados = dados;
        RouteImage = createRoute();
        repaint();
    }

    private java.awt.image.BufferedImage createCar() throws IOException {
        BufferedImage imagemFundo;
        InputStream resource = getClass().getResourceAsStream("/br/com/zebubaja/GpsRoute/Car.png");
        imagemFundo = ImageIO.read(resource);
        return imagemFundo;
    }

    private java.awt.image.BufferedImage createRoute() throws IOException {
        java.awt.GraphicsConfiguration gfxConf = java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        java.awt.image.BufferedImage IMAGE = gfxConf.createCompatibleImage(dados.getGraficoSizeX(),dados.getGraficoSizeY(), java.awt.Transparency.TRANSLUCENT);
        java.awt.Graphics2D g2 = (java.awt.Graphics2D) IMAGE.getGraphics();
        g2.setColor(new java.awt.Color(0xFF0000));
        java.awt.geom.GeneralPath segment_a = new java.awt.geom.GeneralPath();
        int i;
        for (i = 0; i < dados.getSize()-2; i++) {
            segment_a.moveTo( dados.getLatitude(i), dados.getLongitude(i));
            segment_a.lineTo( dados.getLatitude(i + 1), dados.getLongitude(i + 1));
            segment_a.moveTo( dados.getLatitude(i)-2, dados.getLongitude(i)+2);
            segment_a.lineTo( dados.getLatitude(i + 1)+1,dados.getLongitude(i + 1)+1);
            segment_a.moveTo( dados.getLatitude(i), dados.getLongitude(i)+2);
            segment_a.lineTo( dados.getLatitude(i + 1), dados.getLongitude(i + 1)+2);
            g2.draw(segment_a);
            //g2.drawImage(ponto,dados.getLatitude(i),dados.getLongitude(i), null);
        }

        g2.dispose();
        BufferedImage rota;
        rota = new BufferedImage(dados.getSizeX(),dados.getSizeY(), BufferedImage.TRANSLUCENT);
        if((dados.getGraficoSizeX()>dados.getSizeX())&&!(dados.getGraficoSizeY()>dados.getSizeY()))
            rota.getGraphics().drawImage(IMAGE, 0, 0, dados.getSizeX(),dados.getGraficoSizeY(), null);
        else if(!(dados.getGraficoSizeX()>dados.getSizeX())&&(dados.getGraficoSizeY()>dados.getSizeY()))
            rota.getGraphics().drawImage(IMAGE, 0, 0, dados.getGraficoSizeX(),dados.getSizeY(), null); 
        else if((dados.getGraficoSizeX()>dados.getSizeX())&&(dados.getGraficoSizeY()>dados.getSizeY()))
            rota.getGraphics().drawImage(IMAGE, 0, 0, dados.getSizeX(),dados.getSizeY(), null);
        else{            
            return IMAGE;
        }
        return rota;
    }

//    private java.awt.image.BufferedImage createPonto() throws IOException {
//        java.awt.GraphicsConfiguration gfxConf = java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
//        final java.awt.image.BufferedImage IMAGE = gfxConf.createCompatibleImage(6, 6, java.awt.Transparency.TRANSLUCENT);
//
//        java.awt.Graphics2D g2 = IMAGE.createGraphics();
//        java.awt.geom.Point2D FOREGROUND_ON_START = new java.awt.geom.Point2D.Double(0.0, 1.0);
//        java.awt.geom.Point2D FOREGROUND_ON_STOP = new java.awt.geom.Point2D.Double(0.0, 16.0);
//
//        final float[] FOREGROUND_ON_FRACTIONS
//                = {
//                    0.0f,
//                    1.0f
//                };
//
//        final java.awt.Color[] FOREGROUND_ON_COLORS;
//        FOREGROUND_ON_COLORS = new java.awt.Color[]{
//            new java.awt.Color(0x547E00),
//            new java.awt.Color(0xB6FF00)
//        };
//        final java.awt.LinearGradientPaint FOREGROUND_ON_GRADIENT = new java.awt.LinearGradientPaint(FOREGROUND_ON_START, FOREGROUND_ON_STOP, FOREGROUND_ON_FRACTIONS, FOREGROUND_ON_COLORS);
//        final java.awt.geom.Rectangle2D FOREGROUND_ON = new java.awt.geom.Rectangle2D.Double(1, 1, 6, 6);
//        g2.setPaint(FOREGROUND_ON_GRADIENT);
//        g2.fill(FOREGROUND_ON);
//
//        final java.awt.geom.Point2D FOREGROUND_HIGHLIGHT_CENTER = new java.awt.geom.Point2D.Double(6, 7);
//
//        final float[] FOREGROUND_HIGHLIGHT_FRACTIONS
//                = {
//                    0.0f,
//                    0.33f
//                };
//
//        final java.awt.Color[] FOREGROUND_HIGHLIGHT_COLORS
//                = {
//                    new java.awt.Color(1.0f, 1.0f, 1.0f, 0.8f),
//                    new java.awt.Color(1.0f, 1.0f, 1.0f, 0.0f)
//                };
//
//        final java.awt.RadialGradientPaint FOREGROUND_HIGHLIGHT_GRADIENT = new java.awt.RadialGradientPaint(FOREGROUND_HIGHLIGHT_CENTER, 7.0f, FOREGROUND_HIGHLIGHT_FRACTIONS, FOREGROUND_HIGHLIGHT_COLORS);
//
//        final java.awt.geom.Rectangle2D FOREGROUND_HIGHLIGHT = new java.awt.geom.Rectangle2D.Double(1.0, 1.0, 4.0, 8.0);
//
//        g2.setPaint(FOREGROUND_HIGHLIGHT_GRADIENT);
//        g2.dispose();
//        return IMAGE;
//    }

    public void setPosition(int longitude, int latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
        repaint();
    }

    public void setBrightness(luminosity brightness) throws IOException {
        this.brightness = brightness;
        init();
        repaint();
    }

    @Override
    public void setSize(java.awt.Dimension dimension) {
        if (dimension.width >= dimension.height) {
            super.setSize(new java.awt.Dimension(dimension.width, dimension.width));
        } else {
            super.setSize(new java.awt.Dimension(dimension.height, dimension.height));
        }
        repaint();
    }

    @Override
    public void setSize(int width, int height) {
        if (width >= height) {
            super.setSize(width, width);
        } else {
            super.setSize(height, height);
        }
        repaint();
    }

    /**
     * @return the min
     */
    public float getMin() {
        return min;
    }

    /**
     * @param min the min to set
     */
    public void setMin(float min) {
        this.min = min;
    }

    /**
     * @return the max
     */
    public float getMax() {
        return max;
    }

    /**
     * @param max the max to set
     */
    public void setMax(float max) {
        this.max = max;
    }
}
