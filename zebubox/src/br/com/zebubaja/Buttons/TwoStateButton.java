package br.com.zebubaja.Buttons;





import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JButton;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */





/**
 *
 * @author hansolo
 */
public final class TwoStateButton extends JButton 
{
    private java.awt.image.BufferedImage backgroundImage = null;
    java.awt.geom.AffineTransform oldTransform;
    final int altura;
    final int largura;
    private int state=0; 
    private int hover=0;
    private String tipo="conectar";
    public TwoStateButton() throws IOException, URISyntaxException
    {
        super();    
        setPreferredSize(new java.awt.Dimension(100, 100));
        setSize(100,100);
        altura=this.getHeight();
        largura=this.getWidth();
        this.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                try {
                    OnOffButtonMouseEntered(evt);
                } catch (IOException | URISyntaxException ex) {
                    Logger.getLogger(TwoStateButton.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            @Override
            public void mouseExited(java.awt.event.MouseEvent evt) {
                try {
                    OnOffButtonMouseExited(evt);
                } catch (IOException | URISyntaxException ex) {
                    Logger.getLogger(TwoStateButton.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        init();
    }
    public void init() throws IOException, URISyntaxException
    {
        if (backgroundImage != null)
        {
            backgroundImage.flush();
        }
        backgroundImage = Background();
        repaint();
    }
    @Override
    protected void paintComponent(java.awt.Graphics g)
    {
        java.awt.Graphics2D g2 = (java.awt.Graphics2D) g;
        g2.drawImage(backgroundImage, 0, 0, this);
        g2.dispose();
    }
    private java.awt.image.BufferedImage Background() throws IOException
    {
        String diretorio;
        if(hover==0)
            if(state==0)
            diretorio = "/br/com/zebubaja/Buttons/Imagens/"+getTipo()+"off.png";
            else
            diretorio = "/br/com/zebubaja/Buttons/Imagens/"+getTipo()+"on.png";
        else
            if(state==0)
                diretorio = "/br/com/zebubaja/Buttons/Imagens/"+getTipo()+"offhover.png";
            else
                diretorio = "/br/com/zebubaja/Buttons/Imagens/"+getTipo()+"onhover.png";
        InputStream resource = getClass().getResourceAsStream(diretorio);
        BufferedImage imagem=ImageIO.read(resource);
        BufferedImage imagemFundo;
        imagemFundo = new BufferedImage(largura, altura, BufferedImage.TRANSLUCENT);
        imagemFundo.getGraphics().drawImage(imagem, 0, 0, largura, altura, null);
        return imagemFundo;
    }
    private void OnOffButtonMouseExited(MouseEvent evt) throws IOException, URISyntaxException {
        hover=0;
        init();
    }

    private void OnOffButtonMouseEntered(MouseEvent evt) throws IOException, URISyntaxException {
        hover=1;
        init();
    }

    /**
     * @return the state
     */
    public int getState() {
        return state;
    }

    /**
     * @param state the state to set
     * @throws java.io.IOException
     * @throws java.net.URISyntaxException
     */
    public void setState(int state) throws IOException, URISyntaxException {
        this.state = state;
        init();
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
