/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zebubaja.Enum;

/**
 * Este ENUM identifica os tipos de mensagens do xbee no modo API<br>
 * Para maiores detalhes ver <a href="https://www.digi.com/resources/documentation/digidocs/pdfs/90002002.pdf">Datasheet</a>
 * @author Eude Soares Santana <a href="jin.ss.ptu@gmail.com">Eude Soares</a>
 */
public enum ComandType {
    BOX_LED_LIGA('D'),
    BOX_LED_DESLIGA('L'),
    ZEBU_BOX_ALIVE('J');
    public int valor;
    ComandType(int val) {
        valor = val;
    }
    /**
     * <br>
     * @return Retorna valor do ENUM
     */
    public int getValor(){
        return valor;
    }
    
    /**
     * Busca o ENUM correspondente ao valor inteiro<br>
     * @param id Valor que se deseja encontrar o enum
     * @return Retorna ENUM correspondente
     */
    public static ComandType  findType(int id) {
        for(ComandType e : values()) {
            if (e.getValor() == id)
                return e;
        }
        return null;
    }
}
