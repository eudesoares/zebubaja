/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import static sun.misc.PostVMInitHook.run;


/**
 *
 * @author Spider
 */
public class SerialCom {
    protected String[] portas;
    protected Enumeration listaDePortas;
    public String Dadoslidos;
    public int nodeBytes;
    private CommPortIdentifier cp;
    private SerialPort porta;
    private OutputStream saida;
    private boolean Escrita;
    private String Porta;
    protected String peso;
    
    public SerialCom(){
            listaDePortas = CommPortIdentifier.getPortIdentifiers();
    }
    protected void ListarPortas(){
            int i = 0;
            portas = new String[10];
            while (listaDePortas.hasMoreElements()) {
                CommPortIdentifier ips =
                (CommPortIdentifier)listaDePortas.nextElement();
                portas[i] = ips.getName();
                i++;
            }
    }
    public boolean PortaExiste(String COMp){
            String temp;
            boolean e = false;
            while (listaDePortas.hasMoreElements()) {
                CommPortIdentifier ips = (CommPortIdentifier)listaDePortas.nextElement();
                temp = ips.getName();
                if (temp.equals(COMp)== true) {
                e = true;
            }
            }
            return e;
    }
    public void EnviarUmaString(String msg){
            if (Escrita==true) {
                try {
                    saida = porta.getOutputStream();
                    System.out.println("FLUXO OK!");
                } catch (Exception e) {
                    System.out.println("Erro.STATUS: " + e );
                }
                try {
                    System.out.println("Enviando um byte para " + Porta );
                    System.out.println("Enviando : " + msg );
                    saida.write(msg.getBytes());
                    Thread.sleep(100);
                    saida.flush();
                } catch (IOException | InterruptedException e) {
                    System.out.println("Houve um erro durante o envio. ");
                    System.out.println("STATUS: " + e );
                    System.exit(1);
                }
            } else {
                System.exit(1);
            }
    }
}
