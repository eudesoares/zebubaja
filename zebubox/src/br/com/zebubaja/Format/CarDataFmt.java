/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zebubaja.Format;
import br.com.zebubaja.bean.CarData;

/**
 * Tratamento das mensagens de rede, convertendo do formato compacto recebido pelo xbee
 * para o formato de trabalho usando os Beam.
 * @author dinho
 */

public class CarDataFmt {
    int start=13;
    /**
     *  typedef struct data_network{
            char velocidade;     //8bits  Variação de 0 a 255   Km/h
            char v_max;
            short rpm;           //16bits Variação de 0 a 65536 rpm
            short t_cvt;         //16bits 0 a 1024
            short t_motor;       //16bits 0 a 1024
            short tensao;        //16bits 0 a 1024
            short combustivel;   //16bits 0 a 1024
            uint32_t distancia;  //32bits 0 a 2^32 em metros
            uint32_t longitude;  //32 bits longitude
            uint32_t latitude;   //32 bits latitude
        }data_network;
     * Formatação de mensagens da rede de dosadores<br>
     * Formata o vetor de char byte a byte de acordo com o struct formatado nos dosadores
     * O primeiro byte determina o tipo de mensagem da rede dos dosadores.<br>
     * (Maiores detalhes dos vetor de char recebidos <a href="../../../../docs/msg_array.pdf">aqui</a>).
     * @param data Mensagem no formato de bytes recebido pelo xbee.
     * @return Retorna a mensagem formatada no Bean
     */
    public CarData  format(char data[]){
        CarData msg=new CarData();
        int temp;
        msg.setVelocidade(data[start]);
        msg.setVelocidade_maxima(data[start+1]);
        //vazio
        temp=0;
        for(int i=1;i>=0;i--){
            temp=temp<<8;
            temp|=(data[start+3+i]);
        }       
        msg.setRpm(0);
        msg.setLed_box(data[start+2]);
        temp=0;
        for(int i=1;i>=0;i--){
            temp=temp<<8;
            temp|=(data[start+5+i]);
        }       
        msg.setCvt_temp(temp/10+20);
        temp=0;
        for(int i=1;i>=0;i--){
            temp=temp<<8;
            temp|=(data[start+7+i]);
        } 
        msg.setMotor_temp(temp/10+20);  
        temp=0;
         for(int i=1;i>=0;i--){
            temp=temp<<8;
            temp|=(data[start+9+i]);
        }       
        msg.setTensao(temp/10);
        temp=0;
        for(int i=1;i>=0;i--){
            temp=temp<<8;
            temp|=(data[start+11+i]);
        }            
        msg.setNivel_combustivel(temp);
        return msg;
    }
    
}
