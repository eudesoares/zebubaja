/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zebubaja.bean;

import java.util.ArrayList;

/**
 *
 * @author Spider
 */
public class Gpsplot {
    private final  ArrayList<Integer> latitude=new ArrayList();
    private final  ArrayList<Integer> longitude=new ArrayList();
    private int min_longitude;
    private int max_longitude;
    private int min_latitude;
    private int max_latitude;
    private int sizeX;
    private int sizeY;
    private int graficoSizeX;
    private int graficoSizeY;
    private int size=0;
    private int last_latitude=-999999999;
    private int last_longitude=-99999999;
    public boolean addcoord(int latitude,int longitude) {
        latitude=Math.abs(latitude);
        longitude=Math.abs(longitude);
        if(size==0){
            min_latitude=latitude;
            max_latitude=latitude;
            max_longitude=longitude;
            min_longitude=longitude;
        }
        if((last_latitude==latitude)&&(last_longitude==longitude))
            return false;
        if(latitude>max_latitude)
            max_latitude=latitude;
        if(latitude<min_latitude){
            for(int i=0;i<size;i++)
                this.latitude.set(i,(this.latitude.get(i)+(min_latitude-latitude)));
            min_latitude=latitude;
        }
        this.latitude.add(latitude-min_latitude);
        last_latitude=latitude;
          if(longitude>max_longitude)
            max_longitude=longitude;
        if(longitude<min_longitude){
            for(int i=0;i<size;i++)
               this.longitude.set(i,(this.longitude.get(i)+(min_longitude-longitude)));
            min_longitude=longitude;
        }
        this.longitude.add(longitude-min_longitude);
        last_longitude=longitude;
        this.size=this.latitude.size();
        setGraficoSizeX(max_latitude-min_latitude+20);
        setGraficoSizeY(max_longitude-min_longitude+20);
        return true;
    }

    /**
     * @return the size
     */
    public int getSize() {
        return size;
    }

    /**
     * @param i
     * @return the latitude
     */
    public int getLatitude(int i) {
        return (int) latitude.get(i);
    }

    /**
     * @param i
     * @return the longitude
     */
    public int getLongitude(int i) {
        return longitude.get(i);
    }

    /**
     * @return the sizeX
     */
    public int getSizeX() {
        return sizeX;
    }

    /**
     * @param sizeX the sizeX to set
     */
    public void setSizeX(int sizeX) {
        this.sizeX = sizeX-20;
    }

    /**
     * @return the sizeY
     */
    public int getSizeY() {
        return sizeY;
    }

    /**
     * @param sizeY the sizeY to set
     */
    public void setSizeY(int sizeY) {
        this.sizeY = sizeY-20;
    }

    /**
     * @return the graficoSizeX
     */
    public int getGraficoSizeX() {
        return graficoSizeX;
    }

    /**
     * @param graficoSizeX the graficoSizeX to set
     */
    public void setGraficoSizeX(int graficoSizeX) {
        if(graficoSizeX<0)
            graficoSizeX=graficoSizeX*-1;
        this.graficoSizeX = graficoSizeX;
    }

    /**
     * @return the graficoSizeY
     */
    public int getGraficoSizeY() {
        return graficoSizeY;
    }

    /**
     * @param graficoSizeY the graficoSizeY to set
     */
    public void setGraficoSizeY(int graficoSizeY) {
        if(graficoSizeY<0)
            graficoSizeY=graficoSizeY*-1;
        this.graficoSizeY = graficoSizeY;
    }
}
