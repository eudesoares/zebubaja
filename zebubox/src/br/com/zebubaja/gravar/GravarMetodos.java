/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.zebubaja.gravar;

import br.com.zebubaja.bean.CarData;
import br.com.zebubaja.bean.RecProfile;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 *
 * @author Spider
 */
public class GravarMetodos {

    public void salvarDados() throws IOException {
    }
    public ArrayList<RecProfile> loadProfile() throws IOException {
        ArrayList<RecProfile> profiles = new ArrayList<>();
        String localPath = System.getProperty("user.dir");
        try (FileReader arq = new FileReader(localPath + "\\profile.cfg")) {
            BufferedReader lerArq = new BufferedReader(arq);
            String linha = lerArq.readLine(); // lê a primeira linha
            if(linha == null){
                arq.close();
                return profiles;
            }
            String[] valor = linha.split(";", 2);
            int selected=Integer.valueOf(valor[1]);
            if (Integer.valueOf(valor[0]) != 0) {
                linha = lerArq.readLine();
                while (linha != null) {
                    RecProfile profile = new RecProfile();
                    valor = linha.split(";", 10);
                    profile.setNome(valor[0]);
                    profile.setSelected(selected);
                    profile.setDiretorio(valor[1]);
                    if (Integer.valueOf(valor[2]) != 0) {
                        profile.setM_temp(true);
                    }
                    if (Integer.valueOf(valor[3]) != 0) {
                        profile.setRpm(true);
                    }
                    if (Integer.valueOf(valor[4]) != 0) {
                        profile.setC_temp(true);
                    }
                    if (Integer.valueOf(valor[5]) != 0) {
                        profile.setV_max(true);
                    }
                    if (Integer.valueOf(valor[6]) != 0) {
                        profile.setVelocidade(true);
                    }
                    if (Integer.valueOf(valor[7]) != 0) {
                        profile.setLatitude(true);
                    }
                    if (Integer.valueOf(valor[8]) != 0) {
                        profile.setLongitude(true);
                    }
                    if (Integer.valueOf(valor[2]) != 0) {
                        profile.setTensao(true);
                    }
                    linha = lerArq.readLine(); // lê da segunda até a última linha
                    profiles.add(profile);
                }
            }
            arq.close();
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
        }
        
        return profiles;
    }
    public boolean addProfile(ArrayList<RecProfile> profiles) throws IOException{
        String localPath = System.getProperty("user.dir");
        try (FileWriter arq = new FileWriter((localPath + "\\profile.cfg")); 
                PrintWriter gravarArq = new PrintWriter(arq)) {
                if(profiles.isEmpty())
                    gravarArq.print("0;0");
                else{
                    gravarArq.print(profiles.size()+";"+profiles.get(0).getSelected());
                    for(int i=0;i<profiles.size();i++){
                    gravarArq.print("\n"+profiles.get(i).getNome()+";"+profiles.get(i).getDiretorio()+profiles.get(i).getEstados());
                        }
            }
            arq.close();
        }        
        return true;
    }
    public String novoArquivo(RecProfile profile) throws IOException{
        Locale locale = new Locale("pt","BR");
        GregorianCalendar calendar = new GregorianCalendar(); 
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss",locale);
        String file=(profile.getDiretorio()+"//"+profile.getNome()+"_"+format.format(calendar.getTime())+".csv");
                try (FileWriter arq = new FileWriter(file); 
                PrintWriter gravarArq = new PrintWriter(arq)) {
                if(profile.isVelocidade())
                    gravarArq.print("Velocidade;");
                if(profile.isV_max())
                    gravarArq.print("Velocidade_MAX;");
                if(profile.isRpm())
                    gravarArq.print("RPM;");
                if(profile.isM_temp())
                    gravarArq.print("Temperatura_Motor;");
                if(profile.isC_temp())
                    gravarArq.print("Temperatura_CVT;");
                if(profile.isTensao())
                    gravarArq.print("Tensao;");
                if(profile.isLatitude())
                    gravarArq.print("Latitude;");
                if(profile.isLongitude())
                    gravarArq.print("Longitude;");
                gravarArq.println("");
                gravarArq.close();
            }
            return file;
        } 
    public  void addData(CarData dados,RecProfile profile,String file) throws IOException {
                try (FileWriter arq = new FileWriter(file, true); 
                PrintWriter gravarArq = new PrintWriter(arq)) {
                if(profile.isVelocidade())
                    gravarArq.print(dados.getVelocidade()+";");
                if(profile.isV_max())
                    gravarArq.print(dados.getVelocidade_maxima()+";");
                if(profile.isRpm())
                    gravarArq.print(dados.getRpm()+";");
                if(profile.isM_temp())
                    gravarArq.print(dados.getMotor_temp()+";");
                if(profile.isC_temp())
                    gravarArq.print(dados.getCvt_temp()+";");
                if(profile.isTensao())
                    gravarArq.print(dados.getTensao()+";");
                if(profile.isLatitude())
                    gravarArq.print(dados.getLatitude()+";");
                if(profile.isLongitude())
                    gravarArq.print(dados.getLongitude()+";");
                gravarArq.println("");
                gravarArq.close();
                } 
    }
}
