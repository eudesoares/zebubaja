package br.com.zebubaja.instrumentos;





import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JButton;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */





/**
 *
 * @author hansolo
 */
public final class Carangle extends JButton 
{
    private float min = 0;
    private float max = 60;
    private java.awt.image.BufferedImage backgroundImage = null;
    java.awt.geom.AffineTransform oldTransform;
    final int altura=150;
    final int largura=150;
    private int rotationCenterX;
    private int rotationCenterY;
    private double value;
    public Carangle() throws IOException, URISyntaxException
    {
        super();
        setPreferredSize(new java.awt.Dimension(largura, altura));
        setSize(largura,altura);
        init(0);

    }
    private void init(int tipo) throws IOException, URISyntaxException
    {
        if (backgroundImage != null)
        {
            backgroundImage.flush();
        }
        backgroundImage = Background(tipo);
        rotationCenterX = (int) (backgroundImage.getWidth() / 2.0d);
        rotationCenterY = (int) (backgroundImage.getHeight() - backgroundImage.getWidth() / 2.0d);
    }
    @Override
    protected void paintComponent(java.awt.Graphics g)
    {
        java.awt.Graphics2D g2 = (java.awt.Graphics2D) g;
        g2.rotate(value, rotationCenterX, rotationCenterY);
        g2.drawImage(backgroundImage, 0, 0, this);
        g2.dispose();
    }
    private java.awt.image.BufferedImage Background(int tipo) throws IOException
    {
        String diretorio;
        diretorio = "/br/com/zebubaja/Imagens/carangle.png";
        InputStream resource = getClass().getResourceAsStream(diretorio);
        BufferedImage imagem=ImageIO.read(resource);
        BufferedImage imagemFundo;
        imagemFundo = new BufferedImage(largura, altura, BufferedImage.TRANSLUCENT);
        imagemFundo.getGraphics().drawImage(imagem, 0, 0, largura, altura, null);
        return imagemFundo;
    }
    public void setValue(double value)
    {
        if (value < getMin())
        {
            value = getMin();
        }
        if (value > getMax())
        {
            value = getMax();
        }
        this.value = value;
        javax.swing.SwingUtilities.invokeLater(this::repaint);
    }

    /**
     * @return the min
     */
    public float getMin() {
        return min;
    }

    /**
     * @param min the min to set
     */
    public void setMin(float min) {
        this.min = min;
    }

    /**
     * @return the max
     */
    public float getMax() {
        return max;
    }

    /**
     * @param max the max to set
     */
    public void setMax(float max) {
        this.max = max;
    }
}
