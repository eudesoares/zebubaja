/*************************************************************************************************************
                                  DESENHA A MOLDURA TA TELA PRINCIPAL
 ************************************************************************************************************/
void GLCD_Main() {
  ////////////////////Ajusta o mostrador de  Temperatura do motor //////////////////////////////////////////
  GLCD.DrawBitmap(motor_temp_simbol, 0, 0);       // Desenha o simbolo da temperatura do motor sobre a barra
  GLCD.DrawRect(0, 11, 10, 52);                   // Desenha o retangulo externo (vazio) - temp bar
  //////////////////////Ajusta o mostrador de  Temperatura do CVT //////////////////////////////////////////
  GLCD.DrawBitmap(cvt_temp_simbol, 117, 0);       // Desenha o simbolo do motor
  GLCD.DrawRect(117, 11, 10, 52);                 // Desenha o retangulo externo (vazio) - temp bar
  ////////////////////////  Ajusta a barra superior do display /////////////////////////////////////////////
  GLCD.DrawRoundRect(18, 0, 92, 10, 4);           // Desenha o retangulo externo, vazio
  t1.CursorToXY(66, 2);                           // Posiçãoo do divisor do retangulo
  t1.Printf_P(PSTR("|"));                         // Divisor do retangulo
  t1.CursorToXY(22, 2);                           // posição£o no display
  t1.Printf_P(PSTR("     Km"));                   // desenha o formato e unidade
  /////////////////////Edita o mostrador de Velocidade Instantanea//////////////////////////////////////////
  t2.CursorToXY(48, 21);
  t2.Printf_P(PSTR("%02d"), carData.velocidade);         // escreve no display a velocidade
  t1.CursorToXY(80, 42);
  t1.Printf_P(PSTR("Km/h"));
}

/*************************************************************************************************************
                                                2 TELA DO LCD
 ************************************************************************************************************/
void GLCD_Info_update() {
  t1.CursorToXY(41, 4);
  t1.Printf_P(PSTR("%02d"), carData.v_max);
  t1.CursorToXY(41, 16);
  t1.Printf_P(PSTR("%02d.%d"), carData.tensao / 100);
  t1.CursorToXY(48, 28);
  t1.Printf_P(PSTR("%03d"), carData.t_motor / 100);
  t1.CursorToXY(48, 40);
  t1.Printf_P(PSTR("%03d"), carData.t_cvt / 100);
  t1.CursorToXY(66, 52);
  t1.Printf_P(PSTR("%04d"), carData.rpm);
}

/*************************************************************************************************************
                                                2 TELA DO LCD
 ************************************************************************************************************/
void GLCD_2_tela() {
  t1.SelectFont(System5x7);
  t2.SelectFont(fixednums15x31);
  GLCD.Init();
  t1.CursorToXY(0, 4);
  t1.Printf_P(PSTR("V.MAX: 00 Km/h"));
  t1.CursorToXY(0, 16);
  t1.Printf_P(PSTR("V.BAT: 00.0 V\n"));
  t1.CursorToXY(0, 28);
  t1.Printf_P(PSTR("T. MOT: 00 C"));
  t1.CursorToXY(0, 40);
  t1.Printf_P(PSTR("T. CVT: 00 C"));
  t1.CursorToXY(0, 52);
  t1.Printf_P(PSTR("Rot Motor: 0000 RPM"));
}

/*************************************************************************************************************
                        CHECAGEM DE SINALIZAÇÕES DE ERRO NA BASE DO DISPLAY
 ************************************************************************************************************/
void GLCD_error_update() {
  int pos=0;
  if (!pisca_state) {
    pos = 0;                                                                                                 // controle de posição
    if (io_com_state) {
      GLCD.DrawBitmap(io_com_simbol, simbolX[pos], simbolY);                                                 // Desenha o simbolo de falha na comunicação
      pos++;
    }
    ///////////////////////////Mostra simbolo da tensão da bateria ///////////////////////////////////////////
    if (tensao_dsp) {
      GLCD.DrawBitmap(tensao_simbol, simbolX[pos], simbolY);                                                 // Desenha o simbolo da bateria
      pos++;
    }
    ////////////////////////////// Símbolo de nível de combustivel ///////////////////////////////////////////
    if (comb_nivel_dsp) {
      GLCD.DrawBitmap(fuel_simbol, simbolX[pos], simbolY);                                                   // Desenha o simbolo de baixo nivel de combustivel
      pos++;
    }
    if (motor_temp_erro) {
      GLCD.DrawBitmap(motor_temp_simbol2, simbolX[pos], simbolY);                                            // Desenha o simbolo de alta temperatura do motor
      pos++;
    }
    if (cvt_temp_erro) {
      GLCD.DrawBitmap(cvt_temp_simbol2, simbolX[pos], simbolY);                                              // Desenha o simbolo de alta temperatura do cvt
      pos++;
    }
    pisca_state = 1;
  }
  else {
    int cont;
    for (cont = 0; cont < 4; cont++)
      GLCD.DrawBitmap(limpa_simbol, simbolX[cont], simbolY);                                                   // Desenha o simbolo de baixo nivel de combustivel
    pisca_state = 0;
  }
  pisca = millis();
}

/*************************************************************************************************************
                          Atualiza a velocidade na tela inicial do display
 ************************************************************************************************************/
void velo_update() {
  //Verifica se houve diferença para ganhar velocidade na atualização e se a tela atual é a principal
  if ((velo_ant != carData.velocidade) && (page == 0)) {
    if (carData.velocidade > 70) {                                      // Define velocidade máxima de 70km/h
      carData.velocidade = 70;
    }
    if (carData.velocidade < 0) {                                       // Define velocidade mínima de 2km/h
      carData.velocidade = 0;
    }
    t2.CursorToXY(48, 21);
    t2.Printf_P(PSTR("%02d"), carData.velocidade);                      // escreve no display a velocidade
    velo_ant == carData.velocidade;
  }
}

/*************************************************************************************************************
                                  MENSAGEM DE ERRO DO COMBUSTÍVEL
 ************************************************************************************************************/
bool error_mensagem() {
  // Mensagem de erro do Combustível
  if ((comb_nivel_dsp) && !erro_mensagem_status && !erro_mensagem_clear) {   
    erro_mensagem_clear = 0;
    error_msn_time = millis();
    erro_mensagem_status = 1;
    pisca = millis() - 1000;
    pisca_state = 0;
  }
  else if (erro_mensagem_status && (!comb_nivel_dsp||(millis()-error_msn_time)>erro_msn_duration)&&!erro_mensagem_clear) {
    erro_mensagem_clear = 1;                           // sinaliza para apagar a mensgem de tela cheia
    GLCD.ClearScreen();
    GLCD_Main();                                       // Tela principal
    return false;
  } else if (erro_mensagem_clear || !erro_mensagem_status) {
    if (!comb_nivel_dsp) {
      erro_mensagem_clear = 0;
      erro_mensagem_status = 0;
    }
    return false;
  }
  if (((millis() - pisca) > erro_msn_pisca_on) && (pisca_state == 1)) {
    GLCD.ClearScreen();
    pisca_state = 0;
    pisca = millis();
  }
  if (((millis() - pisca) > erro_msn_pisca_off) && (pisca_state == 0)) {
    //if(tensao_dsp)
    if (comb_nivel_dsp)
      GLCD.DrawBitmap(low_fuel_alert, 0, 0); //Desenha o simbolo do motor
    //if(motor_temp_dsp)
    pisca_state = 1;
    pisca = millis();
  }
  return true;
}

/*************************************************************************************************************
                         ATUALIZA AS INFORMAÇÕES NA TELA DO DISPLAY
 ************************************************************************************************************/
void GLCD_Main_Update() {
  if ((set_data == 0) && (button_read() == 15)) {                // se b1 for pressionado por 5 segundos
    set_data = 1;                                                // seleciona rotina de configuraçao
  }
  ////////////////////Ajusta o mostrador de  Temperatura do motor //////////////////////////////////////////
  motor_temp_dsp = 45-(int)(45*(carData.t_motor-motor_min_range)/(motor_max_range-motor_min_range));           // altura da barra em pontos - (altura da barra em pontos * leitura-minimo)/ range
  if(motor_temp_dsp > 45){                                                                                // bar range max
   motor_temp_dsp = 45;
  }
  if(motor_temp_dsp < 0){                                                                                 // bar range min
   motor_temp_dsp = 0;
  }  
  if (motor_temp_erro && !pisca_state) {               // com falha
    GLCD.FillRect(1, 17, 8, 45, WHITE);                // Desenha o retangulo externo (vazio) - temp bar
  }
  else {                                               // sem falha
    if (motor_temp_dsp != motor_temp_dsp2) {
      motor_temp_dsp2 = motor_temp_dsp;
      GLCD.FillRect(1, 17, 8, motor_temp_dsp, WHITE);  // Desenha o retangulo externo (vazio) - temp bar
      // preenche o retangulo interno de acordo com a temp
      GLCD.FillRect(1, motor_temp_dsp + 18, 8, (45 - motor_temp_dsp), BLACK);
    }
  }
  //////////////////////Ajusta o mostrador de  Temperatura do CVT ////////////////////////////////////////// 
  cvt_temp_dsp = 45-(int)(45*(carData.t_cvt-cvt_min_range)/(cvt_max_range-cvt_min_range));           // altura da barra em pontos - (altura da barra em pontos * leitura-minimo)/ range
  if(cvt_temp_dsp > 45){                                                                                // bar range max
   cvt_temp_dsp = 45;
  }
  if(cvt_temp_dsp < 0){                                                                                 // bar range min
   cvt_temp_dsp = 0;
  if (cvt_temp_erro && !pisca_state) {                 // com falha
    GLCD.FillRect(118, 17, 8, 45, WHITE);              // Desenha o retangulo externo (vazio) - temp bar
  }
  else {                                               // sem falha
    if (cvt_temp_dsp != cvt_temp_dsp2) {
      cvt_temp_dsp2 = cvt_temp_dsp;
      GLCD.FillRect(118, 17, 8, cvt_temp_dsp, WHITE);  // Desenha o retangulo externo, vazio
      GLCD.FillRect(118, 18 + cvt_temp_dsp, 8, 45 - cvt_temp_dsp, BLACK);
    }                                          // preenche o retangulo interno de acorto com a temp
  }
  ///////////////////////////Atualiza a Distancia Percorrida ///////////////////////////////////////////////
  t1.CursorToXY(26, 2);                                // posição no display
  t1.Printf_P(PSTR("%02d.%d"), (float)carData.distancia);
  }
} 



