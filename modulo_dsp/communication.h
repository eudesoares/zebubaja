/*************************************************************************************************************
                              LEITURA DOS BOTÕES DO VOLANTE QUE CONFIGURAM O RELÓGIO
 ************************************************************************************************************/
int button_read() {
  if ((!digitalRead(2)) && (!digitalRead(13))) {                               //
    b1_set = b2_set = 3;
    if (((millis() - B3_time) > 1000) && (!b3_set)) {
      b3_set = 1;
      return 305;
    }
    return 0;
  }
  if (digitalRead(2) && ((millis() - B1_time) > 200)) {                        //
    b1_set = 0;
    B1_time = millis();
  }
  if (digitalRead(13) && ((millis() - B2_time) > 200)) {                       //
    b2_set = 0;
    B2_time = millis();
  }
  if (!digitalRead(2) && (b1_set == 0) && ((millis() - B1_time) > 100)) {      //
    b1_set = 1;
    return 103;
  }
  if (!digitalRead(13) && (b2_set == 0) && ((millis() - B2_time) > 100)) {     //
    b2_set = 1;
    return 203;
  }
  else {                                                                       //
    B3_time = millis();
    b3_set = 0;
  }
  //  if((millis()-B1_time) < time_setup){
  //      return 15;
  //  }
  //  if(((millis()-B1_time)>10)&&((millis()-B1_time)<1500)&&(b1_set==1)){
  //      b1_set=3;
  //      return 103;
  //  }
  //  if(((millis()-B2_time)>10)&&((millis()-B2_time)<1500)&&(b2_set==1)){
  //      b1_set=3;
  //      return 203;
  //  }
  return 0;
}
/*************************************************************************************************************
                                                3 TELA DO LCD
 ************************************************************************************************************/

/*************************************************************************************************************
                                       RECEPÇÃO DE DADOS DA SERIAL
 ************************************************************************************************************/
bool leituraMsgCar(data_network* data){
  static XbeeMsg msg;                         //Cria uma mensagem para o xbee
  msg.data=(uint8_t*)data;                     //Passa o ponteiro do struct de dados do veículo
  int status_msg=xbee_read_msg(&msg);         //Captura o status da recepção do de dados do xbee
  switch(status_msg){                         //Verifica o status da recepção de pacotes de mensagens
    case STATUS_OK:                           //Recepção de mensagem foi um sucesso                   
      switch(msg.type){                       //Verifica o status da recepção de pacotes de mensagens
        case RECEIVED_PACKAGE:                //Verifica o tipo de pacote recebido    
          return true;     
        break;
      }
      break;
    case STATUS_ERR_TIMEOUT:                  //Excedeu o tempo de espera pelo pacote
      break;
    case STATUS_ERR_BAD_DATA:                 //Falha na verificação do checksum do pacote recebido.
      break;  
  }
  return false; 
}

bool sendMsgCar(data_network* data){
  uint8_t ans1 = xbee_send_msg(addressBox,(uint8_t*)data,sizeof(*data));
  //uint8_t ans2 = xbee_send_msg(addressPad,(uint8_t*)data,sizeof(*data));
}

